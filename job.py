from net import DeepNet, AdvNet
from net import TorchNet
from train import Train
from tensorflow.keras.callbacks import Callback
import os
from datetime import datetime
import torch


class Job(object):
    def describe(self): return self.__class__.__name__
    def __init__(self, name, problem, nfold, train_fold, epochs, hidden_Nlayer, hidden_Nnode, lr, momentum, output, activation, dropout_rate, batch_size, parallel, multithreads, language, lr_finder, lr_min, lr_max, para_train={}):
        self.nfold = int(nfold)
        self.problem = int(problem)
        self.train_fold = int(train_fold)
        self.epochs = int(epochs)
        self.hidden_Nlayer = int(hidden_Nlayer)
        self.hidden_Nnode = int(hidden_Nnode)
        self.lr = float(lr)
        self.momentum = float(momentum)
        self.activation = activation
        self.dropout_rate = float(dropout_rate)
        self.batch_size = int(batch_size)
        self.parallel = int(parallel)
        self.multithreads = int(multithreads)
        if bool(self.multithreads): assert( not bool(self.parallel))
        self.language = str(language).lower()
        self.lr_finder = bool(int(lr_finder))
        self.lr_range = [float(lr_min), float(lr_max)]
        self.name = self.output if name is None else name
        self.output = f'job_{self.name}__l{self.hidden_Nlayer}n{self.hidden_Nnode}_lr{self.lr}mom{self.momentum}_{self.activation}_k{self.nfold}_dp{self.dropout_rate}_e{self.epochs}_plb{self.problem}_BS{self.batch_size}_parallel{(int)(self.parallel)}__multithreads{(int)(self.multithreads)}_{self.language}' if output is None else output

        self.para_train = para_train
        para_train['base_directory'] = self.output
        print('\033[92m[INFO]\033[0m', '\033[92mJobname \033[0m', self.output, 'parallel:', parallel)

    def run(self):

        ''' An instance of Train for data handling '''
        self.trainer = Train(**self.para_train)
        self.trainer.split(nfold = self.nfold)

        ''' An instance of DeepNet for network construction and pass it to Train '''
        if self.language == 'keras':
            self.deepnet = DeepNet(name = self.name, problem = self.problem, build_dis = False, hidden_Nlayer = self.hidden_Nlayer, hidden_Nnode = self.hidden_Nnode, hidden_activation = self.activation, dropout_rate = self.dropout_rate, parallel = self.parallel)
            # tf.distribute doesn't work on TF1.13 (https://github.com/tensorflow/tensorflow/issues/28931#issuecomment-496748146), wait for TF1.14
            if self.parallel == -1:
                import tensorflow as tf
                #strategy = tf.distribute.MirroredStrategy()
                strategy = tf.distribute.experimental.MultiWorkerMirroredStrategy()
                with strategy.scope():
                    self.deepnet.build(input_dimension = self.trainer.shape, lr = self.lr, momentum = self.momentum)
            else:
                self.deepnet.build(input_dimension = self.trainer.shape, lr = self.lr, momentum = self.momentum)
            self.deepnet.plot(base_directory = self.output)
            self.trainer.setNetwork(self.deepnet.generator)
        elif self.language == 'pytorch':
            self.deepnet = TorchNet(name = self.name, input_dimension = self.trainer.shape, problem = self.problem, hidden_Nlayer = self.hidden_Nlayer, hidden_Nnode = self.hidden_Nnode, dropout_rate = self.dropout_rate, parallel = self.parallel)
            self.trainer.setNetwork(self.deepnet)
            #from torchsummary import summary
            #summary(self.deepnet, input_size)
            #def count_parameters(model):
            #    return sum(p.numel() for p in model.parameters() if p.requires_grad)
            def count_parameters(model):
                import numpy as np
                total_param = 0
                for name, param in model.named_parameters():
                    if param.requires_grad:
                        num_param = np.prod(param.size())
                        if param.dim() > 1:
                            print(name, ':', 'x'.join(str(x) for x in list(param.size())), '=', num_param)
                        else:
                            print(name, ':', num_param)
                        total_param += num_param
                return total_param
            print('\033[92m[INFO]\033[0m', '\033[1;32;40mModel parameters', count_parameters(self.deepnet), '\033[0m')
            print(self.deepnet)
        
        ''' Run the training '''
        T1 = datetime.now()
        if self.language == 'keras':
            if self.lr_finder:
                print('\033[92m[INFO]\033[0m', '\033[1;32;40mLearning rate finder, epoch =', self.epochs, '\033[0m')
                self.result = self.trainer.train_Keras(mode = -1, epochs = self.epochs, fold = self.train_fold, batch_size = self.batch_size, lr_range = self.lr_range)
            else:
                self.result = self.trainer.train_Keras(mode = (1 if self.multithreads == 0 else 4), epochs = self.epochs, fold = self.train_fold, batch_size = self.batch_size, lr_range = self.lr_range)
                for layer in self.deepnet.generator.layers:
                    try:
                        weights = layer.get_weights()[0]
                        biases = layer.get_weights()[1]
                        counts = np.prod(weights.shape) + np.prod(biases.shape)
                        print('Layer weights', np.prod(weights.shape), 'bias', np.prod(biases.shape))
                    except:
                         print('Layer has no weights')
        elif self.language == 'pytorch':
            self.result = self.trainer.train_Torch(lr = self.lr, epochs = self.epochs, fold = self.train_fold, batch_size = self.batch_size)
            if torch.cuda.is_available(): torch.cuda.synchronize()
        T2 = datetime.now()
        print(T2.strftime('%Y-%m-%d %H:%M:%S'), '\033[92m[INFO]\033[0m', '\033[1;32;40mAll-training', self.output, 'takes', str(T2-T1), '\033[0m')
        logfile=open(os.uname()[1] + '.log', 'a+')
        logfile.write(T2.strftime('%Y-%m-%d %H:%M:%S')+' [INFO] '+' All-training '+self.output+' takes '+str(T2-T1)+'\n')
        detaillogfile=open(self.output+'/' + os.uname()[1] + '.detail', 'a+')
        detaillogfile.write(datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' [INFO] '+' All-training '+self.output+' takes '+str(T2-T1)+'\n')
        if self.language == 'keras' and (not self.multithreads):
            self.trainer.plotLoss(self.result)
            self.trainer.plotResults(language = self.language)
        if self.language == 'pytorch':
            self.trainer.plotResults(language = self.language)

        self.saveModel(self.trainer.output_path + self.trainer.name)

#        if self.language == 'keras':
#            trace = timeline.Timeline(step_stats=self.deepnet.run_metadata.step_stats)
#            with open(prefix + '_timeline_ctf.json', 'w') as f:
#                f.write(trace.generate_chrome_trace_format())

    def saveModel(self, prefix):
        # serialize model to JSON
        model_json = self.trainer.network.to_json()
        with open(prefix + '.json', 'w') as json_file:
            json_file.write(model_json)
        # serialize weights to HDF5
        self.trainer.network.save_weights(prefix + '.h5')
        print('Saved', prefix, 'to disk')

class JobAdv(Job):
    def __init__(self, preTrain_epochs, hidden_auxNlayer, hidden_auxNnode, n_iteraction, lam, alr, amomentum, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        self.preTrain_epochs = int(preTrain_epochs)
        self.hidden_auxNlayer = int(hidden_auxNlayer)
        self.hidden_auxNnode = int(hidden_auxNnode)
        self.n_iteraction = int(n_iteraction)
        self.lam = float(lam)
        self.alr = float(alr)
        self.amomentum = float(amomentum)
        self.output = f'{self.output}__E{self.preTrain_epochs}_L{self.hidden_auxNlayer}N{self.hidden_auxNnode}_alr{self.alr}mom{self.amomentum}_it{self.n_iteraction}_Loss{self.problem}_lam{self.lam}'
        self.para_train['base_directory'] = self.output
        print('\033[92m[INFO]\033[0m', '\033[92mJobname \033[0m', self.output)
        
    def run(self):

        ''' An instance of Train for data handling '''
        self.trainer = Train(**self.para_train)
        self.trainer.split(nfold = self.nfold)

        ''' An instance of AdvNet for network construction and pass it to Train '''
        self.advnet = AdvNet(name = self.name, problem = self.problem, build_dis = True, hidden_Nlayer = self.hidden_Nlayer, hidden_Nnode = self.hidden_Nnode,
            hidden_activation = self.activation, hidden_auxNlayer = self.hidden_auxNlayer, hidden_auxNnode = self.hidden_auxNnode, dropout_rate = self.dropout_rate, parallel = self.parallel)
        self.advnet.build(input_dimension = self.trainer.shape, lam = self.lam, lr = self.lr, momentum = self.momentum, alr = self.alr, amomentum = self.amomentum)
        self.advnet.plot(base_directory = self.output)

        class Evaluate(Callback):
            def __init__(self, name, trainer, mode):
                self.name = name
                self.trainer = trainer
                self.mode = mode
            def on_epoch_end(self, epoch, logs):
                pass
                #print('\033[92m[INFO]\033[0m', '\033[92mCheckpoint \033[0m', self.name, epoch, self.mode, logs)
#                self.trainer.plotResults(self.name + str(epoch), mode)
    
        ''' pre-training '''
        T1 = datetime.now()
        if self.preTrain_epochs != 0:
            prefix = 'pre-gen'
            print('\033[92m[INFO]\033[0m', '\033[92mpre-training generator (1st) with epochs\033[0m', self.preTrain_epochs)
            AdvNet.make_trainable(self.advnet.discriminator, False)
            AdvNet.make_trainable(self.advnet.generator, True)
            self.trainer.setNetwork(self.advnet.generator)
            t1 = datetime.now()
            self.result = self.trainer.train_Keras(mode = 1, epochs = self.preTrain_epochs, fold = self.train_fold, batch_size = self.batch_size, callbacks=[Evaluate(prefix, self.trainer, 'y')])
            t2 = datetime.now()
            print('\033[92m[INFO]\033[0m', '\033[1;32;40mpre-training generator (1st) takes', str(t2-t1), '\033[0m')
            detaillogfile=open(self.output+'/' + os.uname()[1] + '.detail', 'a+')
            detaillogfile.write(datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' [INFO] '+' pre-training generator (1st) '+str(self.preTrain_epochs)+' epochs '+self.output+' takes '+str(t2-t1)+'\n')
            self.trainer.plotLoss(self.result, prefix)
            #self.trainer.plotResults(prefix, 'y')

            prefix = 'pre-dis'
            dis_preTrain_epochs = 1
            print('\033[92m[INFO]\033[0m', '\033[92mpre-training discriminator (2nd) with epochs\033[0m', dis_preTrain_epochs)
            AdvNet.make_trainable(self.advnet.discriminator, True)
            AdvNet.make_trainable(self.advnet.generator, False)
            self.trainer.setNetwork(self.advnet.discriminator)
            t1 = datetime.now()
            self.result = self.trainer.train_Keras(mode = 2, epochs = dis_preTrain_epochs, fold = self.train_fold, batch_size = self.batch_size, callbacks=[Evaluate(prefix, self.trainer, 'z')])
            t2 = datetime.now()
            print('\033[92m[INFO]\033[0m', '\033[1;32;40mpre-training discriminator (2nd) takes', str(t2-t1), '\033[0m')
            detaillogfile=open(self.output+'/' + os.uname()[1] + '.detail', 'a+')
            detaillogfile.write(datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' [INFO] '+' pre-training disciming (2nd) '+str(dis_preTrain_epochs)+' epochs '+self.output+' takes '+str(t2-t1)+'\n')
            self.trainer.plotLoss(self.result, prefix, True)
            #self.trainer.plotResults(prefix, 'z')
        else:
            print('\033[91m[INFO]\033[0m', '\033[91mpre-training skipped!\033[0m')

        self.output_path = '/'.join([self.output, self.describe()]) + '/'
        if not os.path.exists(self.output_path):
            os.makedirs(self.output_path)
        ''' Iterative training '''
        for i in range(1, self.n_iteraction+1):

            print('\033[92m[INFO] Going to train\033[0m', i, '\033[92miteration, generator (1st) with epochs\033[0m', self.epochs)
            AdvNet.make_trainable(self.advnet.discriminator, False)
            AdvNet.make_trainable(self.advnet.generator, True)
            self.trainer.setNetwork(self.advnet.adversary)
            t1 = datetime.now()
            self.result = self.trainer.train_Keras(mode = 3, epochs = self.epochs, fold = self.train_fold, batch_size = self.batch_size, callbacks=[Evaluate(prefix, self.trainer, 'yz')])
            t2 = datetime.now()
            print('\033[92m[INFO]\033[0m', '\033[1;32;40miter-training generator (1st) takes', str(t2-t1), '\033[0m')
            detaillogfile=open(self.output+'/' + os.uname()[1] + '.detail', 'a+')
            detaillogfile.write(datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' [INFO] '+' it'+str(i)+ '-training generator (1st) '+str(self.epochs)+' epochs '+self.output+' takes '+str(t2-t1)+'\n')

            AdvNet.make_trainable(self.advnet.discriminator, True)
            AdvNet.make_trainable(self.advnet.generator, True)
            if i == self.n_iteraction: self.trainer.plotIteration(i)

            prefix = 'iter-gen' + str(i)
            mode = 'y'
            print('\033[92m[DEBUG] Going to inspect predction by\033[0m', prefix, '\033[92mwith mode\033[0m', mode)
            self.trainer.setNetwork(self.advnet.generator)
            #if (i % 5 == 0):
            #    self.trainer.plotResults(prefix, mode)

            #if (i % 5 == 0):
            #    self.saveModel(self.output_path + self.trainer.name + '_' + str(i))

            print('\033[92m[INFO] Going to train\033[0m', i, '\033[92miteration, discriminator (2nd) with epochs\033[0m', 1)
            AdvNet.make_trainable(self.advnet.discriminator, True)
            AdvNet.make_trainable(self.advnet.generator, False)
            self.trainer.setNetwork(self.advnet.discriminator)
            t1 = datetime.now()
            self.result = self.trainer.train_Keras(mode = 2, epochs = 1, fold = self.train_fold, batch_size = self.batch_size, callbacks=[Evaluate(prefix, self.trainer, 'z')])
            t2 = datetime.now()
            print('\033[92m[INFO]\033[0m', '\033[1;32;40miter-training discriminator (2nd) takes', str(t2-t1), '\033[0m')
            detaillogfile=open(self.output+'/' + os.uname()[1] + '.detail', 'a+')
            detaillogfile.write(datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' [INFO] '+' it'+str(i)+'-training discrimin (2nd) '+str(self.epochs)+' epochs '+self.output+' takes '+str(t2-t1)+'\n')

            prefix = 'iter-dis' + str(i)
            mode = 'z'
            print('\033[92m[INFO] Going to inspect predction by\033[0m', prefix, '\033[92mwith mode\033[0m', mode)
            #if (i % 5 == 0) or (i<2):
            #    self.trainer.plotResults(prefix, mode)

            #self.trainer.setNetwork(self.advnet.adversary)
            #self.trainer.plotIteration(i+0.5)

        print('\033[92m[INFO]\033[0m', self.n_iteraction, '\033[92mIteration done, storing and plotting results.\033[0m')
        T2 = datetime.now()
        s = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        print(s, '\033[92m[INFO]\033[0m', '\033[1;32;40mAll-training', self.output, 'takes', str(T2-T1), '\033[0m')
        logfile=open(os.uname()[1] + '.log', 'a+')
        logfile.write(s+' [INFO] '+' All-training '+self.output+' takes '+str(T2-T1)+'\n')
        detaillogfile=open(self.output+'/' + os.uname()[1] + '.detail', 'a+')
        detaillogfile.write(s+' [INFO] '+' All-training '+self.output+' takes '+str(T2-T1)+'\n')
        self.trainer.setNetwork(self.advnet.adversary)
        self.trainer.saveLoss()
        self.trainer.setNetwork(self.advnet.generator)
        self.trainer.plotResults('iter-genFinal')

