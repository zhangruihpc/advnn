from job import Job, JobAdv
import itertools    
from itertools import chain
import os

def update_dict(orig_dict, new_dict):
    for k, v in new_dict.items():
        if k in orig_dict:
            orig_dict[k] = v

class Batch(object):
    def __init__(self, jobname, base_directory, inputs):
        self.jobname = jobname

        self.base_directory = base_directory + '/'
        self.para_train_sim = {'name': '2j2b',
            'signal_h5': 'sig_zero_jet.h5',
            'signal_name': 'sig',
            'signal_tree': 'AppInputs',
            'backgd_h5': 'bkg_zero_jet.h5',
            'backgd_name': 'bkg',
            'backgd_tree': 'AppInputs',
            'weight_name': 'weight',
            'variables': ['Z_PT_FSR', 'Z_Y_FSR', 'Muons_CosThetaStar'],
            }
        update_dict(self.para_train_sim, inputs)

        self.para_net_sim = {
            'name': 'simple',
            'problem': '0',
            'nfold': 3,
            'train_fold': 0,
            'epochs': 100,
            'hidden_Nlayer': 10,
            'hidden_Nnode': 100,
            'lr': 0.03,
            'momentum': 0.8,
            'output': None,
            'activation': 'elu',
            'dropout_rate': '0.0',
            'batch_size': 5000,
            'parallel': 0,
            'multithreads': 0,
            'language': 'keras',
            'lr_finder': 0,
            'lr_min': 1e-3,
            'lr_max': 1e-2,
            }
        update_dict(self.para_net_sim, inputs)

        self.para_train_Adv = {**self.para_train_sim,
            'name': 'NP',
            'has_syst': True,
            'syssig_h5': '/Users/zhangrui/Work/Code/ML/ANN/h5files/tW_DS_2j2b.h5',
            'syssig_name': 'tW_DS',
            'syssig_tree': 'wt_DS',
            }
        update_dict(self.para_train_Adv, inputs)

        self.para_net_Adv = {**self.para_net_sim,
            'name': 'ANN',
            'problem': '0',
            'epochs': 2,
            'hidden_auxNlayer': 2,
            'hidden_auxNnode': 5,
            'preTrain_epochs': 20,
            'n_iteraction': 100,
            'alr': 0.9,
            'amomentum': 0.5,
            'lam': 10,
        }
        update_dict(self.para_net_Adv, inputs)

        self.para_train_AdvReg = {**self.para_train_sim,
            'name': 'Mass',
            'has_mass': True,
            'reg_variable': 'm_mumu',
            'reg_latex': r'm_\mu\mu',
            }
        update_dict(self.para_train_AdvReg, inputs)

        self.para_net_AdvReg = {**self.para_net_sim,
            'name': 'ANNReg',
            'problem': '1',
            'epochs': 2,
            'hidden_auxNlayer': 2,
            'hidden_auxNnode': 5,
            'preTrain_epochs': 20,
            'n_iteraction': 100,
            'alr': 0.9,
            'amomentum': 0.5,
            'lam': 10,
        }
        update_dict(self.para_net_AdvReg, inputs)

        self.wrappe = self.base_directory + self.jobname + '_wrap.sh'
        self.htcjdl = self.base_directory + self.jobname + '_htc.jdl'

        self.req_memory = 6
        self.req_cores = 6

    def create_jdl(self, job_array, local_run = False):
        log = 'log'
        if not os.path.exists('/'.join([self.base_directory, log])):
            os.makedirs('/'.join([self.base_directory, log]))
        def prefix_jdl():
                ''' Steering file for HTCondor (Prefix). '''

                return '\n'.join(l[20:] for l in """
                    Executable              = {wrappe}
                    Universe                = vanilla
                    Transfer_executable     = True
                    Request_memory          = {req_memory}
                    Request_cpus            = {req_cores}
                    # Disk space in kiB, if no unit is specified!
                    Request_disk            = 4 GB
                    +JobFlavour             = "workday"
                    +AccountingGroup        = "group_u_ATLASWISC.all"
                    # Specify job input and output
                    Error                   = {log}/err.$(ClusterId).$(Process)
                    Output                  = {log}/out.$(ClusterId).$(Process)
                    Log                     = {log}/log.$(ClusterId).$(Process)
                    # Additional job requirements (note the plus signs)
                    # Choose OS (options: "SL6", "CentOS7", "Ubuntu1804")
                    +ContainerOS            = "SL6"
                    """.split('\n')[1:]).format(
                        wrappe = self.wrappe,
                        req_memory = '{} GB'.format(self.req_memory),
                        req_cores = str(self.req_cores),
                        log = log)


        def jobarr_jdl(setting):
            ''' Steering file for HTCondor (Job array). '''

            return '\n'.join(l[20:] for l in """
                    
                    # Submit 1 job
                    arguments               = {arguments}
                    Queue 1
                    """.split('\n')[1:]).format(
                        arguments = ' '.join(list(chain(*setting.items()))))

        if not local_run:
            with open(self.htcjdl, 'w+') as f:
                f.write(prefix_jdl())

        def product_dict(**kwargs):
            keys = kwargs.keys()
            vals = kwargs.values()
            for instance in itertools.product(*vals):
                yield dict(zip(keys, instance))
                
        settings = list(product_dict(**job_array))
        print('\033[92m[INFO]\033[0m', len(settings), '\033[92mjobs will be created.\033[0m')
        for setting in settings:
            if not local_run:
                with open(self.htcjdl, 'a+') as f:
                    f.write(jobarr_jdl(setting))
            elif self.jobname == 'ANN':
                # If local run adversarial neural network
                update_dict(self.para_net_Adv, setting)
                job = JobAdv(**self.para_net_Adv, para_train = self.para_train_Adv)
                job.run()
            elif self.jobname == 'ANNReg':
                # If local run adversarial neural network
                update_dict(self.para_net_AdvReg, setting)
                job = JobAdv(**self.para_net_AdvReg, para_train = self.para_train_AdvReg)
                job.run()
            else:
                update_dict(self.para_net_sim, setting)
                job = Job(**self.para_net_sim, para_train = self.para_train_sim)
                job.run()

    def create_wrap(self):

        def wrapper():
            ''' Executable file for HTCondor. '''

            return '\n'.join(l[8:] for l in """
        #!/bin/bash
        source ~/.bashrc
        conda activate twaml
        dest={base_directory}
        source $dest../.venvs/twaml-venv-lxpus7/bin/activate
        region=$1
        shift
        python {program}/submit.py {mode} $region _run $*
        \cp -r job_* $dest/
        unset dest
        """.split('\n')[1:]).format(
                base_directory = self.base_directory,
                program = os.path.dirname(os.path.abspath(__file__)),
                mode = self.jobname)

        with open(self.wrappe, 'w+') as f:
            f.write(wrapper())


    def _run(self, param):
        setting = {param[i]: param[i+1] for i in range(0, len(param), 2)}
        if self.jobname == 'ANN':
            update_dict(self.para_net_Adv, setting)
            job = JobAdv(**self.para_net_Adv, para_train = self.para_train_Adv)
            job.run()
        elif self.jobname == 'ANNReg':
            update_dict(self.para_net_AdvReg, setting)
            job = JobAdv(**self.para_net_AdvReg, para_train = self.para_train_AdvReg)
            job.run()
        else:

            update_dict(self.para_net_sim, setting)
            job = Job(**self.para_net_sim, para_train = self.para_train_sim)
            job.run()

    def opt(self, param):
        #skopt
        import skopt
        from skopt import gbrt_minimize, gp_minimize
        from skopt.space import Real, Categorical, Integer
        from skopt import Optimizer
        from skopt import dump

        # Define objective
        def objective(setting):
            setting.update({'lr_finder': 0})
            setting.update({'epochs': 2})
            update_dict(self.para_net_sim, setting)
            job = Job(**self.para_net_sim, para_train = self.para_train_sim)
            job.run()
            loss_train, loss_test, loss_hold = job.trainer.evaluate()
            return loss_test[0]

        if param[0] == 'opt':
            region = str(param[1])
            nlayer = (3,6)
            nnode = (20,100)
            if '0j' == region:
                nlayer = (1,4)
                nnode = (10,60)
            search_space  = [Real(0, 0.2, name='dropout_rate'),
            Categorical(['relu', 'elu'], name='activation'),
            Real(10**-4, 0.1, 'log-uniform', name='lr'),
            Real(0.7, 0.9, name='momentum'),
            Integer(nlayer[0], nlayer[1], name='hidden_Nlayer'),
            Integer(nnode[0], nnode[1], name='hidden_Nnode'),
            ]
            search_name = [var.name for var in search_space]

            opt = Optimizer(search_space, # TODO: Add noise
                        n_initial_points=10,
                        acq_optimizer_kwargs={'n_jobs':4})
            exp_dir = 'model/'
            if 'CONTINUE' == param[3] and os.path.isfile(exp_dir + 'optimizer.pkl'):
                from skopt import load
                opt = load(exp_dir + 'optimizer.pkl')
            
            #gp_result = gp_minimize(func=objective,
            #                dimensions=space,
            #                n_calls=100,
            #                noise= 0.01,
            #                n_jobs=4,)

            # Begin the process of as many workers as there are visible GPUs
            n_calls = int(param[2])
            if not os.path.exists(exp_dir):
                    os.makedirs(exp_dir)

            for i in range(n_calls):
                sampled_point = opt.ask()
                print('point', i+1, sampled_point)
                f_val = objective(dict(zip(search_name, sampled_point)))
                opt_result = opt.tell(sampled_point, f_val)

                with open(exp_dir + 'optimizer.pkl', 'wb') as f:
                    dump(opt, f)
                with open(exp_dir + 'results.pkl', 'wb') as f:
                    # Delete objective function before saving. To be used when results have been loaded,
                    # the objective function must then be imported from this script.
                    if opt_result.specs is not None:
                        if 'func' in opt_result.specs['args']:
                            res_without_func = copy.deepcopy(opt_result)
                            del res_without_func.specs['args']['func']
                            dump(res_without_func, f)
                        else:
                            dump(opt_result, f)
                    else:
                        dump(opt_result, f)

        elif param[0] == 'plot':
            import matplotlib.pyplot as plt
            from skopt import load
            #from skopt.plots import plot_objective, plot_evaluations, plot_convergence
            from plots_with_cat import plot_objective, plot_evaluations, plot_convergence, plot_regret

            plt.switch_backend('agg') # For outputting plots when running on a server
            plt.ioff() # Turn off interactive mode, so that figures are only saved, not displayed
            plt.style.use('seaborn-paper')
            font_size = 9
            label_font_size = 8
            plt.rcParams.update({'font.size'        : font_size,
                     'axes.titlesize'   : font_size,
                     'axes.labelsize'   : font_size,
                     'xtick.labelsize'  : label_font_size,
                     'ytick.labelsize'  : label_font_size,
                     'figure.figsize'   : (5,4)}) # w,h

            fig_dir = 'model/figures/'
            if not os.path.exists(fig_dir):
                os.makedirs(fig_dir)

            # Plots expect default rcParams
            plt.rcdefaults()
            
            # Load results
            res_loaded = load('model/results.pkl')
            print(res_loaded)
            
            # Plot evaluations
            fig,ax = plt.subplots()
            ax = plot_evaluations(res_loaded)
            plt.tight_layout()
            plt.savefig(fig_dir + f'evaluations.pdf')
            
            # Plot objective
            fig,ax = plt.subplots()
            ax = plot_objective(res_loaded)
            plt.tight_layout()
            plt.savefig(fig_dir + f'objective.pdf')
            
            # Plot convergence
            fig,ax = plt.subplots()
            ax = plot_convergence(res_loaded)
            plt.tight_layout()
            plt.savefig(fig_dir + f'convergence.pdf')

            # Plot regret
            fig,ax = plt.subplots()
            ax = plot_regret(res_loaded)
            plt.tight_layout()
            plt.savefig(fig_dir + f'regret.pdf')
            
            print(f'Saved skopt figures in {fig_dir}.')
