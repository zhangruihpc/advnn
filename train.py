import numpy as np
import tensorflow.keras as keras
from sklearn.model_selection import KFold
from sklearn.metrics import roc_auc_score
from data import from_pytables
from data import scale_weight_sum
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import os
from sklearn.preprocessing import StandardScaler
import pickle
from datetime import datetime

import torch
from torch.utils.data import Dataset, DataLoader
from torch import nn, optim
import torch.nn.functional as F
import copy
from callbacks import LRFinder, SGDRScheduler, CyclicLR, OneCycleLR
from keras.callbacks import EarlyStopping

#from numpy.random import seed
#seed(678)
#from tensorflow import set_random_seed
#set_random_seed(123)

# https://discuss.pytorch.org/t/input-numpy-ndarray-instead-of-images-in-a-cnn/18797/2
class MyDataset(Dataset):
    ''' Transform numpy array to pyTorch Dataset '''
    def __init__(self, data, target, transform=None):
        self.data = torch.from_numpy(data).float()
        self.target = torch.from_numpy(target).long()
        self.transform = transform
        
    def __getitem__(self, index):
        x = self.data[index]
        y = self.target[index]
        
        if self.transform:
            x = self.transform(x)
        
        return x, y

    def __len__(self):
        return len(self.data)


class Train(object):
    def describe(self): return self.__class__.__name__
    def __init__(self, name = 'zero_jet', base_directory = '../h5', signal_h5 = 'sig_one_jet.h5', signal_name = 'sig', signal_tree = 'wt_DR_nominal', signal_latex = r'H$\rightarrow\mu\mu$',
            backgd_h5 = 'bkg_zero_jet.h5', backgd_name = 'bkg', backgd_tree = 'tt_nominal', backgd_latex =  r'Data sideband', weight_name = 'weight',
            variables = ['Z_PT_FSR', 'Z_Y_FSR', 'Muons_CosThetaStar', 'm_mumu'],
            has_syst = False, syssig_h5 = 'tW_DS_2j2b.h5', syssig_name = 'tW_DS_2j2b', syssig_tree = 'tW_DS', syssig_latex = r'$tW$ DS',
            has_mass = False, reg_variable = 'm_mumu', reg_latex = r'm_\mu\mu',
            ):
        self.name = name
        self.signal_label, self.backgd_label, self.center_label, self.syssig_label = 1, 0, 1, 0
        self.signal_latex, self.backgd_latex = signal_latex, backgd_latex
        self.signal = from_pytables(signal_h5, signal_name, tree_name = signal_tree, weight_name = weight_name, label = self.signal_label, auxlabel = self.center_label)
        self.backgd = from_pytables(backgd_h5, backgd_name, tree_name = backgd_tree, weight_name = weight_name, label = self.backgd_label, auxlabel = self.center_label)
        self.signal.keep_columns(variables)
        self.backgd.keep_columns(variables)
        self.has_syst = has_syst
        self.syssig_latex = None if not self.has_syst else syssig_latex
        self.losses_test = {'L_gen': [], 'L_dis': [], 'L_diff': []}
        self.losses_train = {'L_gen': [], 'L_dis': [], 'L_diff': []}
        self.has_mass = has_mass
        self.reg_variable = reg_variable
        self.reg_latex = reg_latex

        if self.has_syst:
            self.syssig = from_pytables(syssig_h5, syssig_name, tree_name = syssig_tree, weight_name = weight_name, label = self.signal_label, auxlabel = self.syssig_label)
            self.syssig.keep_columns(variables)

            # Append syssig to signal
            self.signal.append(self.syssig)


        # Equalise signal weights to background weights
        scale_weight_sum(self.signal, self.backgd)
        self.X_raw = np.concatenate([self.signal.df.to_numpy(), self.backgd.df.to_numpy()])
        scaler = StandardScaler()
        self.X = scaler.fit_transform(self.X_raw)
        self.y = np.concatenate([self.signal.label_asarray(), self.backgd.label_asarray()])
        self.z = np.concatenate([self.signal.auxlabel_asarray(), self.backgd.auxlabel_asarray()])
        if self.has_mass:
            signal = from_pytables(signal_h5, signal_name, tree_name = signal_tree, weight_name = weight_name, label = self.signal_label, auxlabel = self.center_label)
            backgd = from_pytables(backgd_h5, backgd_name, tree_name = backgd_tree, weight_name = weight_name, label = self.backgd_label, auxlabel = self.center_label)
            signal.keep_columns([reg_variable])
            backgd.keep_columns([reg_variable])
            def normalise(df):
                df[df > 200] = 200
                return (df-110)/70.
            self.z = np.concatenate([normalise(signal.df).to_numpy(), normalise(backgd.df).to_numpy()])
        self.w = np.concatenate([self.signal.weights, self.backgd.weights])

        self.output_path = '/'.join([base_directory, self.describe()]) + '/'
        if not os.path.exists(self.output_path):
            os.makedirs(self.output_path)
        print('\033[92m[INFO]\033[0m signal/background', self.describe(), self.signal.df.__getitem__, self.backgd.df.__getitem__)
        print('\033[92m[INFO]\033[0m', '-'*20)

        #store the content
        with open(self.output_path + self.name + '_event.pkl', 'wb') as pkl:
            pickle.dump(scaler, pkl)
        #store the content
        with open(self.output_path + self.name + '_event_py2.pkl', 'wb') as pkl:
            pickle.dump(scaler, pkl, protocol=2)

    @property
    def shape(self):
        return self.signal.shape[1]

    def setNetwork(self, net):
        self.network = net

    def split(self, nfold, seed = 666):
        ''' Split sample to training and test portions using KFold '''

        kfolder_hold = KFold(n_splits = 5, shuffle = True, random_state = seed-1)
        for rest_idx, hold_idx in kfolder_hold.split(self.X):
            self.X_rest, self.X_hold = self.X[rest_idx], self.X[hold_idx]
            self.y_rest, self.y_hold = self.y[rest_idx], self.y[hold_idx]
            self.z_rest, self.z_hold = self.z[rest_idx], self.z[hold_idx]
            self.w_rest, self.w_hold = self.w[rest_idx], self.w[hold_idx]

        self.nfold = nfold
        kfolder_test = KFold(n_splits = self.nfold, shuffle = True, random_state = seed)
        self.X_train, self.X_test = {}, {}
        self.y_train, self.y_test = {}, {}
        self.z_train, self.z_test = {}, {}
        self.w_train, self.w_test = {}, {}
        for i, (train_idx, test_idx) in enumerate(kfolder_test.split(self.X_rest)):
            self.X_train[i], self.X_test[i] = self.X_rest[train_idx], self.X_rest[test_idx]
            self.y_train[i], self.y_test[i] = self.y_rest[train_idx], self.y_rest[test_idx]
            self.z_train[i], self.z_test[i] = self.z_rest[train_idx], self.z_rest[test_idx]
            self.w_train[i], self.w_test[i] = self.w_rest[train_idx], self.w_rest[test_idx]

    def train_Keras(self, mode, epochs, fold, batch_size, lr_range, callbacks = []):
        '''
        mode = -1: one target mode => signal vs backgd (lr finder)
               0: one target mode => signal vs backgd
               1: one target mode => signal vs backgd (no validation; useful for `tf.distribute.MirroredStrategy()`
               2: one target mode => signal vs syssig
               3: two target mode => signal+syssig vs backgd and signal+backgd vs syssig
               4: one target mode => signal vs backgd (no validation; useful for multi-threads models)`
        '''
        self.epochs = epochs
        self.fold = fold
        self.batch_size = batch_size
        self.lr_range = lr_range

        if mode == -1:
            _lrf = LRFinder(min_lr=self.lr_range[0],
                       max_lr=self.lr_range[1],
                       scale='linear',
                       steps_per_epoch=np.ceil(self.X_train[self.fold].shape[0]/self.batch_size),
                       epochs=self.epochs,
                       fig_dir= self.output_path)
            result = self.network.fit(self.X_train[self.fold], self.y_train[self.fold], sample_weight = self.w_train[self.fold], batch_size = self.batch_size,
                validation_data = (self.X_test[self.fold], self.y_test[self.fold], self.w_test[self.fold]), epochs = self.epochs, callbacks=[_lrf])
            # Plot learning rate finder results, with chosen upper and lower learning rate
            _lrf.plot_loss_vs_lr(chosen_limits=None)
            _lrf.plot_lr()
            return result

        elif mode == 0:
            callbacks.push_back(EarlyStopping(monitor='val_loss', mode='min', verbose=1))
            callbacks.push_back(keras.callbacks.ModelCheckpoint(self.output_path + self.name + '_model_{epoch:04d}.h5', period=int(self.epochs/10.)))
            callbacks.push_back(OneCycleLR(base_lr=self.lr_range[0], max_lr=self.lr_range[1]))

            # serialize model to JSON
            model_json = self.network.to_json()
            with open(self.output_path + self.name + '_model.json', 'w') as json_file:
                json_file.write(model_json)
            return self.network.fit(self.X_train[self.fold], self.y_train[self.fold], sample_weight = self.w_train[self.fold], batch_size = self.batch_size,
                validation_data = (self.X_test[self.fold], self.y_test[self.fold], self.w_test[self.fold]), epochs = self.epochs, callbacks=callbacks)
        elif mode == 1:
            return self.network.fit(self.X_train[self.fold], self.y_train[self.fold], sample_weight = self.w_train[self.fold], batch_size = self.batch_size,
                epochs = self.epochs, callbacks=callbacks)
        elif mode == 2:
            assert (self.has_syst or self.has_mass)
            tensorboard_callback = keras.callbacks.TensorBoard(log_dir=self.output_path + self.name + 'mode' + str(mode) + datetime.now().strftime("%Y%m%d-%H%M%S") + '_tfboard', histogram_freq=1)
            return self.network.fit(self.X_train[self.fold], self.z_train[self.fold], sample_weight = self.w_train[self.fold], batch_size = self.batch_size,
                #validation_data = (self.X_test[self.fold], self.z_test[self.fold], self.w_test[self.fold]), epochs = self.epochs, callbacks=[tensorboard_callback])
                validation_data = (self.X_test[self.fold], self.z_test[self.fold], self.w_test[self.fold]), epochs = self.epochs, callbacks=callbacks)
        elif mode == 3:
            assert (self.has_syst or self.has_mass)
            tensorboard_callback = keras.callbacks.TensorBoard(log_dir=self.output_path + self.name + 'mode' + str(mode) + datetime.now().strftime("%Y%m%d-%H%M%S") + '_tfboard', histogram_freq=1)
            return self.network.fit(self.X_train[self.fold],  [self.y_train[self.fold], self.z_train[self.fold]], sample_weight = [self.w_train[self.fold], self.w_train[self.fold]], batch_size = self.batch_size,
                #validation_data = (self.X_test[self.fold], [self.y_test[self.fold], self.z_test[self.fold]], [self.w_test[self.fold], self.w_test[self.fold]]), epochs = self.epochs, callbacks=[tensorboard_callback])
                validation_data = (self.X_test[self.fold], [self.y_test[self.fold], self.z_test[self.fold]], [self.w_test[self.fold], self.w_test[self.fold]]), epochs = self.epochs, callbacks=callbacks)
        elif mode == 4:
            import tensorflow as tf
            from tensorflow.keras.optimizers import SGD
            def my_function(device, lr, momentum, i=0):
                with tf.device(device):
                    model = keras.models.clone_model(self.network)
                    sgd = SGD(lr = lr, momentum = momentum)
                    model.compile(loss = 'binary_crossentropy', optimizer = sgd, metrics=['accuracy'])
                    result = model.fit(self.X_train[self.fold], self.y_train[self.fold], sample_weight = self.w_train[self.fold], batch_size = self.batch_size, epochs = self.epochs)
                return result

            ''' multithreads = True: using 4 threads to send 4 models to 4 GPUs
                multithreads = False: run 4 models on 1 GPU sequentially '''
            multithreads = False
            devices = []
            ngpu = 4
            devices.append('GPU:0')
            devices.append('GPU:1')
            devices.append('GPU:2')
            devices.append('GPU:3')

            if multithreads:
                lr = [keras.backend.eval(self.network.optimizer.lr) for i in range(ngpu)]
                momentum = [keras.backend.eval(self.network.optimizer.momentum) for i in range(ngpu)]
                ''' Two ways to implement multithreading '''
                #import threading
                #threads = [ threading.Thread(target=my_function, args=(devices[i],lr[i],momentum[i])) for i in range(ngpu) ]
                #for t in threads:
                #    t.start()
                #for t in threads:
                #    t.join()

                from multiprocessing.dummy import Pool as ThreadPool 
                pool = ThreadPool(ngpu) 
                results = []
                results = pool.starmap(my_function, zip(devices, range(ngpu), lr, momentum))

            else:
                results = []
                lr = keras.backend.eval(self.network.optimizer.lr)
                momentum = keras.backend.eval(self.network.optimizer.momentum)
                for i in range(ngpu):
                    results.append(my_function(devices[i], lr, momentum))

    def evaluate(self, mode = 'y'):
        print('\033[92m[INFO] Evaluating by net:\033[0m', self.network.name, '\033[92madversary\033[0m', self.has_syst or self.has_mass, self.name, '\033[92mmode\033[0m', mode)
        if mode.lower() == 'y':
            loss_train = self.network.evaluate(self.X_train[self.fold], self.y_train[self.fold], sample_weight = self.w_train[self.fold], verbose=0, batch_size=self.batch_size)
            loss_test = self.network.evaluate(self.X_test[self.fold], self.y_test[self.fold], sample_weight = self.w_test[self.fold], verbose=0, batch_size=self.batch_size)
            loss_hold = self.network.evaluate(self.X_hold, self.y_hold, sample_weight = self.w_hold, verbose=0, batch_size=self.batch_size)
        elif mode.lower() == 'z':
            loss_train = self.network.evaluate(self.X_train[self.fold], self.z_train[self.fold], sample_weight = self.w_train[self.fold], verbose=0, batch_size=self.batch_size)
            loss_test = self.network.evaluate(self.X_test[self.fold], self.z_test[self.fold], sample_weight = self.w_test[self.fold], verbose=0, batch_size=self.batch_size)
            loss_hold = self.network.evaluate(self.X_hold, self.z_hold, sample_weight = self.w_hold, verbose=0, batch_size=self.batch_size)
        elif mode.lower() == 'yz':
            loss_train = self.network.evaluate(self.X_train[self.fold],  [self.y_train[self.fold], self.z_train[self.fold]], sample_weight = [self.w_train[self.fold], self.w_train[self.fold]], verbose=0, batch_size=self.batch_size)
            loss_test = self.network.evaluate(self.X_test[self.fold], [self.y_test[self.fold], self.z_test[self.fold]], sample_weight = [self.w_test[self.fold], self.w_test[self.fold]], verbose=0, batch_size=self.batch_size)
            loss_hold = self.network.evaluate(self.X_hold, [self.y_hold, self.z_hold], sample_weight = [self.w_hold, self.w_hold], verbose=0, batch_size=self.batch_size)
            t2 = datetime.now()
        return loss_train, loss_test, loss_hold

    def train_Torch(self, lr, epochs, fold, batch_size, callbacks = ''):
        torch.backends.cudnn.benchmark = True
        self.epochs = epochs
        self.fold = fold
        self.batch_size = batch_size

        def my_function(device, i=0):
            model = copy.deepcopy(self.network)
            model.to(device)
            #print(next(model.parameters()).device)

            criterion = nn.CrossEntropyLoss()
            optimizer = optim.SGD(model.parameters(), lr=lr+i*lr)
            
            dataset_train = MyDataset(self.X_train[self.fold], self.y_train[self.fold])
            loader = DataLoader(dataset_train, batch_size = int(self.batch_size/(i+1)), shuffle = False, num_workers = 0, pin_memory = torch.cuda.is_available())


            begin = datetime.now()
            for epoch in range(self.epochs):
                print(datetime.now(), '\033[92m[INFO]\033[0m', '\033[92mEpoch:', epoch, '\033[0m', i, device)
                #if i == 0: continue
                for id, data in enumerate(loader):
                    X = data[0].to(device)
                    y = data[1].to(device)
                    out = model(X)
                    loss = criterion(out, y)
                
                    optimizer.zero_grad()
                    loss.backward()
                    optimizer.step()
            torch.save(model, f'model{i}.pt')

        ''' multithreads = True: using 4 threads to send 4 models to 4 GPUs
            multithreads = False: run 4 models on 1 GPU sequentially '''
        multithreads = False
        devices = []
        ngpu = 4
        devices.append(torch.device('cuda:0' if torch.cuda.is_available() else 'cpu'))
        devices.append(torch.device('cuda:1' if torch.cuda.is_available() else None))
        devices.append(torch.device('cuda:2' if torch.cuda.is_available() else None))
        devices.append(torch.device('cuda:3' if torch.cuda.is_available() else None))

        if multithreads:
            from multiprocessing.dummy import Pool as ThreadPool 
            pool = ThreadPool(ngpu) 
            results = pool.starmap(my_function, zip(devices, range(ngpu)))
            #results = pool.map(my_function, devices)
        else:
            results = []
            for i, item in enumerate(devices):
                results.append(my_function(item, i))
        

    def plotLoss(self, result, name = 'sim', regression = False):
        ''' Plot loss functions '''
        if self.epochs < 2:
            print('Only', self.epochs, 'epochs, no need for plotLoss.')
            return

        if regression:
            # Summarise history for mse
            plt.plot(result.history['mean_squared_error'])
            if 'val_mean_squared_error' in result.history: plt.plot(result.history['val_mean_squared_error'])
            plt.title('network loss')
            plt.ylabel('MSE')
            plt.xlabel('Epoch')
            plt.legend(['Train', 'Test'], loc='upper right')
            plt.savefig(self.output_path + self.name + '_' + name + '_mse' + '.pdf', format='pdf')
            plt.clf()
            # Summarise history for loss
            plt.plot(result.history['loss'])
            if 'val_loss' in result.history: plt.plot(result.history['val_loss'])
            plt.title('network loss')
            plt.ylabel('Loss')
            plt.xlabel('Epoch')
            plt.legend(['Train', 'Test'], loc='upper right')
            plt.savefig(self.output_path + self.name + '_' + name + '_rloss' + '.pdf', format='pdf')
            plt.clf()

        else:
            # Summarise history for accuracy
            plt.plot(result.history['acc'])
            if 'val_acc' in result.history: plt.plot(result.history['val_acc'])
            plt.title('network accuracy')
            plt.ylabel('Accuracy')
            plt.xlabel('Epoch')
            plt.legend(['Train', 'Test'], loc='upper left')
            plt.savefig(self.output_path + self.name + '_' + name + '_acc' + '.pdf', format='pdf')
            plt.clf()
            # Summarise history for loss
            plt.plot(result.history['loss'])
            if 'val_loss' in result.history: plt.plot(result.history['val_loss'])
            plt.title('network loss')
            plt.ylabel('Loss')
            plt.xlabel('Epoch')
            plt.legend(['Train', 'Test'], loc='upper right')
            plt.savefig(self.output_path + self.name + '_' + name + '_loss' + '.pdf', format='pdf')
            plt.clf()


    def plotResults(self, name = 'sim', mode = 'y', xlo = 0., xhi = 1, nbin = 20, language = 'keras'):
        from sklearn.metrics import roc_curve, auc

        print('\033[92m[INFO] Predicting by net:\033[0m', self.network.name, '\033[92mwih mode\033[0m', mode)
        if language == 'keras':
            t1 = datetime.now()
            train_predict = self.network.predict(self.X_train[self.fold], batch_size=self.batch_size)
            t2 = datetime.now()

            t3 = datetime.now()
            test_predict = self.network.predict(self.X_test[self.fold], batch_size=self.batch_size)
            t4 = datetime.now()

            hold_predict = self.network.predict(self.X_hold, batch_size=self.batch_size)

        elif language == 'pytorch':
            self.network.eval()
            train_predict, test_predict = [], []
            dataset_train = MyDataset(self.X_train[self.fold], self.y_train[self.fold])
            loader_train = DataLoader(dataset_train, batch_size = self.batch_size, shuffle = False, num_workers = 0, pin_memory = torch.cuda.is_available())
            dataset_test = MyDataset(self.X_test[self.fold], self.y_test[self.fold])
            loader_test = DataLoader(dataset_test, batch_size = self.batch_size, shuffle = False, num_workers = 0, pin_memory = torch.cuda.is_available())
            return

            with torch.no_grad():
                device = torch.device('cuda:2' if torch.cuda.is_available() else 'cpu')
                if torch.cuda.is_available(): torch.cuda.synchronize()
                t1 = datetime.now()
                for X, y in loader_train:
                    X = X.to(device)
                    #y = y.to(device)
                    out = self.network(X)
                    prediction_train = out.detach().cpu().numpy()[:,1]
                    train_predict.extend(prediction_train)
                assert (len(train_predict) == len(self.y_train[self.fold]))
                if torch.cuda.is_available(): torch.cuda.synchronize()
                t2 = datetime.now()

                if torch.cuda.is_available(): torch.cuda.synchronize()
                t3 = datetime.now()
                for X, y in loader_test:
                    X = X.to(device)
                    #y = y.to(device)
                    out = self.network(X)
                    prediction_test = out.detach().cpu().numpy()[:,1]
                    test_predict.extend(prediction_test)
                assert (len(test_predict) == len(self.y_test[self.fold]))
                if torch.cuda.is_available(): torch.cuda.synchronize()
                t4 = datetime.now()
            train_predict = np.array(train_predict)
            test_predict = np.array(test_predict)
            hold_predict = np.array(hold_predict)

        print('\033[92m[INFO]\033[0m', '\033[1;32;40mPredict training sample in', language, 'mode', mode, 'takes', str(t2-t1), '\033[0m')
        detaillogfile=open(self.output_path + self.name + '_' + name + '_' + mode + '_' + os.uname()[1] + '.detail', 'a+')
        detaillogfile.write(datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' [INFO] '+' Predicting training sample in '+str(mode)+' mode takes '+str(t2-t1)+'\n')

        print('\033[92m[INFO]\033[0m', '\033[1;32;40mPredict test     sample in', language, 'mode', mode, 'takes', str(t4-t3), '\033[0m')
        detaillogfile=open(self.output_path + self.name + '_' + name + '_' + mode + '_' + os.uname()[1] + '.detail', 'a+')
        detaillogfile.write(datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' [INFO] '+' Predicting test     sample in '+str(mode)+' mode takes '+str(t2-t1)+'\n')


        train_FP, train_TP, train_TH = roc_curve(self.y_train[self.fold], train_predict, sample_weight=self.w_train[self.fold])
        test_FP, test_TP, test_TH = roc_curve(self.y_test[self.fold], test_predict, sample_weight=self.w_test[self.fold])
        hold_FP, hold_TP, hold_TH = roc_curve(self.y_hold, hold_predict, sample_weight=self.w_hold)
        train_AUC = auc(train_FP, train_TP, reorder=True)
        test_AUC = auc(test_FP, test_TP, reorder=True)
        hold_AUC = auc(hold_FP, hold_TP, reorder=True)
        print('\033[92m[INFO]\033[0m', '\033[1;32;40mPredict results in mode', mode, 'is', train_AUC, test_AUC, hold_AUC, '\033[0m')
        detaillogfile=open(self.output_path + self.name + '_' + name + '_' + mode + '_' + os.uname()[1] + '.detail', 'a+')
        detaillogfile.write(datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' [INFO] '+' Predicting results in '+language+' mode'+str(mode)+' is (train) '+str(train_AUC)+' (test) '+str(test_AUC)+' (hold) '+str(hold_AUC)+'\n')

        plt.title('Receiver Operating Characteristic')
        plt.plot(train_FP, train_TP, 'g--', label='Train AUC = %2.1f%%'% (train_AUC * 100))
        plt.plot(test_FP, test_TP, 'b', label='Val   AUC = %2.1f%%'% (test_AUC * 100))
        plt.plot(hold_FP, hold_TP, 'r--', label='Test  AUC = %2.1f%%'% (hold_AUC * 100))
        plt.legend(loc='lower right')

        plt.plot([0,1],[0,1],'k--')
        plt.xlim([-0.,1.])
        plt.ylim([-0.,1.])
        plt.ylabel('True Positive Rate')
        plt.xlabel('False Positive Rate')
        plt.savefig(self.output_path + self.name + '_' + name + '_' + mode + '_ROC' + '.pdf', format='pdf')
        plt.clf()

        names = ['Absolute', 'Normalised']
        for density in [0, 1]:
            ax = plt.subplot(1, 2, density + 1)

            plt.hist(train_predict[self.y_train[self.fold] == self.signal_label], range = [xlo, xhi], bins = nbin, histtype = 'step', density = density, label='Training ' + self.signal_latex)
            plt.hist(train_predict[self.y_train[self.fold] == self.backgd_label], range = [xlo, xhi], bins = nbin, histtype = 'step', density = density, label='Training ' + self.backgd_latex)
            plt.hist(test_predict[self.y_test[self.fold] == self.signal_label],   range = [xlo, xhi], bins = nbin, histtype = 'step', density = density, label='Val ' + self.signal_latex, linestyle = 'dashed')
            plt.hist(test_predict[self.y_test[self.fold] == self.backgd_label],   range = [xlo, xhi], bins = nbin, histtype = 'step', density = density, label='Val ' + self.backgd_latex, linestyle = 'dashed')
            plt.hist(hold_predict[self.y_hold == self.signal_label],   range = [xlo, xhi], bins = nbin, histtype = 'step', density = density, label='Hold ' + self.signal_latex, linestyle = 'dashed')
            plt.hist(hold_predict[self.y_hold == self.backgd_label],   range = [xlo, xhi], bins = nbin, histtype = 'step', density = density, label='Hold ' + self.backgd_latex, linestyle = 'dashed')
            plt.ylim(0, plt.gca().get_ylim()[1] * 1.5)
            plt.legend(prop={'size': 6})
            plt.xlabel('Response' if mode.lower() == 'y' else 'Mass prediction', horizontalalignment = 'left', fontsize = 'large')
            plt.title(names[density])
            plt.text(0.1, 0.70, 'Train AUC: ' + f"{train_AUC:.2%}", transform=ax.transAxes, fontsize=8)
            plt.text(0.1, 0.75, 'Val AUC: ' + f"{test_AUC:.2%}", transform=ax.transAxes, fontsize=8)
            plt.text(0.1, 0.80, 'Test AUC: ' + f"{hold_AUC:.2%}", transform=ax.transAxes, fontsize=8)
        plt.savefig(self.output_path + self.name + '_' + name + '_' + mode + '_response' + '.pdf', format='pdf')
        plt.clf()

        with open(self.output_path + self.name + '_' + name + '_' + mode + '_ROC' + '.txt', 'w') as f:
            f.write('Train AUC = %2.1f %%\n'% (train_AUC * 100))
            f.write('Val   AUC = %2.1f %%\n'% (test_AUC * 100))
            f.write('Test  AUC = %2.1f %%\n'% (hold_AUC * 100))

        if mode == 'z':
            plt.hist(self.z_train[self.fold]-train_predict, range = [-1, 1], bins = nbin, histtype = 'step', density = 0, label='Training ')
            plt.hist(self.z_test[self.fold]-test_predict,   range = [-1, 1], bins = nbin, histtype = 'step', density = 0, label='Test ', linestyle = 'dashed')
            plt.legend(loc='upper right')
            plt.xlabel('True - Prediction')
            plt.ylabel('Events')
            plt.savefig(self.output_path + self.name + '_' + name + '_' + mode + '_response_residual' + '.pdf', format='pdf')
            plt.clf()


    def plotIteration(self, it):
        if not (self.has_syst or self.has_mass):
            return

        loss_train, loss_test = self.evaluate(mode = 'yz', batch_size=self.batch_size)
        self.losses_test['L_gen'].append(loss_test[1][None][0])
        self.losses_test['L_dis'].append(-loss_test[2][None][0])
        self.losses_test['L_diff'].append(loss_test[0][None][0])
        self.losses_train['L_gen'].append(loss_train[1][None][0])
        self.losses_train['L_dis'].append(-loss_train[2][None][0])
        self.losses_train['L_diff'].append(loss_train[0][None][0])
        print('\033[92m[DEBUG]\033[0m', it, 'loss1: (', self.losses_train['L_gen'][-1], self.losses_test['L_gen'][-1], '); loss2: (', self.losses_train['L_dis'][-1], self.losses_test['L_dis'][-1], '); lossAll: (', self.losses_train['L_diff'][-1], self.losses_test['L_diff'][-1], ')')
        print('\033[92m[DEBUG] Train Loss_gen\033[0m', self.losses_train['L_gen'])
        print('\033[92m[DEBUG] Test  Loss_gen\033[0m', self.losses_test['L_gen'])
        print('\033[92m[DEBUG] Train Loss_dis\033[0m', self.losses_train['L_dis'])
        print('\033[92m[DEBUG] Test  Loss_dis\033[0m', self.losses_test['L_dis'])
        print('\033[92m[DEBUG] Train Loss_diff\033[0m', self.losses_train['L_diff'])
        print('\033[92m[DEBUG] Test  Loss_diff\033[0m', self.losses_test['L_diff'])

        def plot_twolosses():
            idxes = ['L_gen', 'L_dis', 'L_diff']
            latex = [r'$L_{\mathrm{gen}}$', r'$\lambda \cdot L_{\mathrm{dis}}$', r'$L_{\mathrm{gen}} - \lambda \cdot L_{\mathrm{dis}}$']
            for idx in range(len(self.losses_test)):
                ax = plt.subplot(3, 1, idx + 1)

                plt.plot(np.arange(1, len(self.losses_train[idxes[idx]])+1), self.losses_train[idxes[idx]], '', label = r'Train')
                plt.plot(np.arange(1, len(self.losses_train[idxes[idx]])+1), self.losses_test[idxes[idx]], '--', label = r'Test ')

                plt.legend(loc='upper right')
                plt.ylabel(latex[idx], fontsize='large')
                plt.grid()

            plt.xlabel('Number of iterations', horizontalalignment='left', fontsize='large')
            plt.subplots_adjust(left=0.18, right=0.95, top=0.95, hspace = 0.4)
            plt.savefig(self.output_path + self.name + '_iter' + str(it) + '.pdf', format = 'pdf')
            plt.clf()

        if (it % 5 == 0) or (it < 10):
            plot_twolosses()

    def saveLoss(self):
        import json
        with open(self.output_path + self.name + '_iter.json', 'w') as fp:
            print('Saved', fp.name, 'to disk')
            #json.dump(self.losses_test, fp)
            #json.dump(self.losses_train, fp)
